#!/usr/bin/env python

import re
import sys

file_name_input =   sys.argv[1]
font_name       =   sys.argv[2]

with open(f'{file_name_input}', 'r') as file_in:
    contents    =   file_in.read()

font_width      =   re.findall(r'{\n(.+?),', contents)[0]
font_height     =   re.findall(r'\n(.+?), /\* Font height', contents)[0]
char_number     =   re.findall(r'\n(.+?), /\* Number of characters', contents)[0]

if (font_width  ==   '0x00'): # propotional font
    raw_width   =   re.findall(r'/\* Character widths \*/\n(.+?)\n\n', contents, re.DOTALL)[0]

    # trim space chars
    raw_width   =   raw_width.replace('\n', '')
    raw_width   =   raw_width.replace(' ','')

    width       =   []

    for value_hex in raw_width.split(','):
        try:
            width.append(value_hex)
        except ValueError:
            pass
else:
    width       =   [font_width] * char_number

contents        =   re.findall(r'#else.*};', contents, re.DOTALL)[0]
value_char      =   re.findall(r"ASCII Character: '(.+?)'", contents)
value_hex       =   re.findall(r'\' \*/\n(.+?), \n\n', contents, re.DOTALL)

with open(f'{font_name}.py', 'w') as file_out:
    file_out.write(f'# {font_name}\n{font_name} = {{\n')
    file_out.write(f"  'HEIGHT'        :    {font_height},\n\n")

    for i in range(len(value_char)):
        try:
            value_hex[i] = value_hex[i].replace('\n', '')
            if ((value_char[i] == '\\') or (value_char[i] == "'")):
                file_out.write(f"  '\\{value_char[i]}' :  {{'byte' : [{value_hex[i]}], 'width' : {width[i]}}},\n")
            else:
                file_out.write(f"  '{value_char[i]}'  :  {{'byte' : [{value_hex[i]}], 'width' : {width[i]}}},\n")
        except IndexError:
            print(f'{value_hex[i]}')

    file_out.write('}')